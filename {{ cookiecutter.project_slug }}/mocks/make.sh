#!/usr/bin/env bash

mockery --name=Context --srcpkg github.com/labstack/echo/v4 --case underscore
mockery --name=HandlerFunc --srcpkg github.com/labstack/echo/v4 --case underscore
