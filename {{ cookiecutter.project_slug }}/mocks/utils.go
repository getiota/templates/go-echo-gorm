package mocks

import (
	"io/ioutil"
	"os"
	"path"
)

func exists(p string) bool {
	if _, err := os.Stat(p); os.IsNotExist(err) {
		return false
	}
	return true
}

func getFixtureDir() string {
	wd, _ := os.Getwd()

	// Not looking more than 3 deep
	for i := 0; i < 3; i++ {
		fixtureDir := path.Join(wd, "fixtures")
		if exists(fixtureDir) {
			return fixtureDir
		}

		if exists(path.Join(wd, "go.mod")) {
			return ""
		}

		wd = path.Dir(wd)
	}

	return ""
}

func getFixturePath(name string) string {
	return path.Join(getFixtureDir(), name)
}

// Fixture reads a fixture file
func Fixture(name string) []byte {
	bytes, err := ioutil.ReadFile(getFixturePath(name))
	if err != nil {
		panic(err)
	}
	return bytes
}
