module getiota.fr/api/{{ cookiecutter.project_slug }}

go 1.15

require (
	github.com/GoogleCloudPlatform/cloudsql-proxy v1.18.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/rs/zerolog v1.19.0
	github.com/stretchr/testify v1.4.0
	gitlab.com/getiota/go/logging v0.1.2
	gitlab.com/getiota/go/errors v0.1.1
	{%- if cookiecutter.database == "True" %}
	gorm.io/driver/postgres v1.0.2
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.2
	{%- endif %}
)
