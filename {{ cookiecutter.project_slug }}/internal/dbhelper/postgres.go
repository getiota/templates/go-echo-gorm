// +build !sqlite

package dbhelper

import (
	"fmt"
	"os"

	"github.com/rs/zerolog/log"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	// Importing custom cloud proxy dialect
	_ "github.com/GoogleCloudPlatform/cloudsql-proxy/proxy/dialers/postgres"
)

func getEnv(key string) (string, error) {
	if value, ok := os.LookupEnv(key); ok {
		return value, nil
	}
	return "", fmt.Errorf("Missing env `%s`", key)
}

func getHost() (host string, cloudProxy bool, err error) {
	if host, err = getEnv("DB_CONN"); err == nil {
		cloudProxy = true
		return
	}
	host, err = getEnv("DB_HOST")
	return
}

// Open opens the database
func Open() (db *gorm.DB, err error) {
	var host, port, name, user, pass string
	var isCloudProxy bool
	if host, isCloudProxy, err = getHost(); err != nil {
		return
	}
	if port, err = getEnv("DB_PORT"); err != nil {
		return
	}
	if name, err = getEnv("DB_NAME"); err != nil {
		return
	}
	if user, err = getEnv("DB_USER"); err != nil {
		return
	}
	if pass, err = getEnv("DB_PASS"); err != nil {
		return
	}

	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", host, port, user, name, pass,
	)

	if isCloudProxy {
		log.Info().Msgf("Connecting to %s with the cloud proxy", host)
		db, err = gorm.Open(postgres.New(postgres.Config{DriverName: "cloudsqlpostgres", DSN: dsn}), &gorm.Config{})
	} else {
		log.Info().Msgf("Connecting to %s without the cloud proxy", host)
		db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	}
	return
}
