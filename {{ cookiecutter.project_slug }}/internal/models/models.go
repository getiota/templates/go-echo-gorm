package models

import "gorm.io/gorm"

// AutoMigrate performs migrations for all models
func AutoMigrate(db *gorm.DB) error {
	return db.AutoMigrate(
		// &Model{},
	)
}
