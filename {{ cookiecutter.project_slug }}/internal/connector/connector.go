package connector

import "gorm.io/gorm"

// Connector is a layer on top of models that exposes read / write operations
type Connector interface {
}

// DefaultConnector implements all model connections
type DefaultConnector struct {
	db *gorm.DB
}

// NewConnector is the default constructor
func NewConnector(db *gorm.DB) *DefaultConnector {
	return &DefaultConnector{db: db}
}
