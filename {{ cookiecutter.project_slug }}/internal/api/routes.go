package api

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// Hello returns nothing
func Hello(c echo.Context) error {
	return c.NoContent(http.StatusOK)
}
