package api

import (
	{%- if cookiecutter.database == "True"  %}
	"getiota.fr/api/{{ cookiecutter.project_slug }}/internal/connector"
	{%- endif  %}
	"github.com/labstack/echo/v4"
)

// Setup prepares the API
{%- if cookiecutter.database == "True" %}
func Setup(e *echo.Echo, connector connector.Connector) error {
	e.GET("/hello", Hello)
	return nil
}
{%- else %}
func Setup(e *echo.Echo) error {
	e.GET("/hello", Hello)
	return nil
}
{%- endif %}
