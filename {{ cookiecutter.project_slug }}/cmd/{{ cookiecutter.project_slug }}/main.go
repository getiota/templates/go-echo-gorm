package main

import (
	"getiota.fr/api/{{ cookiecutter.project_slug }}/internal/api"
	{%- if cookiecutter.database == "True" %}
	"getiota.fr/api/{{ cookiecutter.project_slug }}/internal/connector"
	"getiota.fr/api/{{ cookiecutter.project_slug }}/internal/dbhelper"
	"getiota.fr/api/{{ cookiecutter.project_slug }}/internal/models"
	{%- endif %}

	"gitlab.com/getiota/go/errors"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/rs/zerolog/log"
	"gitlab.com/getiota/go/logging"
)

var exposedPort string

func main() {
	// ------------------------------------
	// Start Echo Server
	e := echo.New()
	// Prepare logs
	logging.Prepare(e)

	{%- if cookiecutter.database == "True" %}
	// ------------------------------------
	// Open Database or exit
	db, err := dbhelper.Open()
	if err != nil {
		log.Fatal().Err(err).Msg("Could not open database")
		return
	}
	if err = models.AutoMigrate(db); err != nil {
		log.Fatal().Err(err).Msg("Could not migrate database")
	}
	connector := connector.NewConnector(db)

	{%- endif %}

	// Middleware
	e.Use(middleware.Recover())

	// Errors
	e.HTTPErrorHandler = errors.Handler

	// Routes
	{%- if cookiecutter.database == "True"  %}
	err = api.Setup(e, connector)
	{%- else %}
	err := api.Setup(e)
	{%- endif %}
	if err != nil {
		log.Fatal().Err(err).Msg("")
	}

	// Start server
	e.Logger.Fatal(e.Start(":" + exposedPort))
}
