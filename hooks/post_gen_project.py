#!/usr/bin/env python
import os
import shutil
import subprocess
import json

MANIFEST = "manifest.json"

def first_commit():
    subprocess.call(['git', 'init'])
    subprocess.call(['git', 'add', '-A'])
    subprocess.call(['git', 'commit', '-m', 'First commit'])

# From https://github.com/cookiecutter/cookiecutter/issues/723
def delete_resources_for_disabled_features():
    with open(MANIFEST) as manifest_file:
        manifest = json.load(manifest_file)
        for feature in manifest['features']:
            if not feature['enabled']:
                # print "removing resources for disabled feature {}...".format(feature['name'])
                for resource in feature['resources']:
                    delete_resource(resource)
    # print "cleanup complete, removing manifest..."
    delete_resource(MANIFEST)


def delete_resource(resource):
    if os.path.isfile(resource):
        # print "removing file: {}".format(resource)
        os.remove(resource)
    elif os.path.isdir(resource):
        # print "removing directory: {}".format(resource)
        shutil.rmtree(resource)

if __name__ == "__main__":
    delete_resources_for_disabled_features()
    first_commit()

