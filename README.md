# CookieCutter template for a Go/Echo/Gorm project

This is the base structure for a IOTA microservice.

## Usage

Install [Cookie Cutter](https://cookiecutter.readthedocs.io/en/1.7.2/installation.html) and
run `cookiecutter https://gitlab.com/projet-iota/templates/go-echo-gorm`